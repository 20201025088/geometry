//crear una escena 



//THREE
let escena=new THREE.Scene();
//crear camara
let camara= new THREE.PerspectiveCamera(100,window.innerWidth/window.innerHeight, 0.1, 1000);
//crear lienzo
let lienzo= new THREE.WebGLRenderer();
lienzo.setSize(window.innerWidth,window.innerHeight)
document.body.appendChild(lienzo.domElement);
//crear geometria

let circulo= new THREE.ConeGeometry( 2.5, 10, 14);
let material=new THREE.MeshBasicMaterial({color:0x000FF, wireframe: true})

//Crear la malla de la escena 
let micirculo=new THREE.Mesh(circulo,material)
//agregar la malla 

escena.add(micirculo);

camara.position.z=5;

let animar=function(){
    requestAnimationFrame(animar);
    
    micirculo.rotation.x=micirculo.rotation.x+0.03
    micirculo.rotation.y=micirculo.rotation.y+0.03

    lienzo.render(escena,camara);

    if(micirculo.position.x<10 && micirculo.position.y==0){

        micirculo.position.x=micirculo.position.x+0.3
    }else if(micirculo.position.x!=10 && micirculo.position.y<5 && micirculo.position.y==0 ){


        micirculo.position.y=micirculo.position.y+0.3
    }else if (micirculo.position.x>-10 && micirculo.position.y!=5){
        micirculo.position.x=micirculo.position.x-0.3

    }else if(micirculo.position.x!=-10 && micirculo.position.y>-5 && micirculo.position.y>=0 ){
        micirculo.position.y=micirculo.position.y-0.3

    }



    /*if(micirculo.position.y<5 && micirculo.position.x==0){

        micirculo.position.y=micirculo.position.y+0.3
    }else if(micirculo.position.y!=5 && micirculo.position.x<10 && micirculo.position.x==0 ){


        micirculo.position.x=micirculo.position.x+0.3
    }else if (micirculo.position.y>-5 && micirculo.position.x!=10){
        micirculo.position.y=micirculo.position.y-0.3

    }else if(micirculo.position.y!=-5 && micirculo.position.x>-10 && micirculo.position.x>=0 ){
        micirculo.position.x=micirculo.position.x-0.3

    }*/
}
animar();